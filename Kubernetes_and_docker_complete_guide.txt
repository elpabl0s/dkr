Removing Stopped Container
~~~~~~~~~~~~~~~~~~~~~~~~~~

docker system prune


Retrieving Log Outputs
~~~~~~~~~~~~~~~~~~~~~~

docker logs ID <container id>

Stopping Containers
~~~~~~~~~~~~~~~~~~~

Stop a Container (waiting 10 seconds)
docker stop <container id>

Kill a Container (instantly)
docker kill <container id>

Execute ad aditional command in a container
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

docker exec -it <container id> <command>

Getting a Command Prompt in a Container
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

docker exec -it <container id> sh
docker run -it <image> sh

Docker Compose Commands
~~~~~~~~~~~~~~~~~~~~~~~

docker-compose up
docker-compose down

rebuild container
docker-compose up --build 

Automatic Container Restart
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Restart Policies
"no" - Never attempt to restart this . container if it stops or crashes

always - If this container stops *for any reason* always attempt to restart it

on-failure - Only restart if the container stops with an error code

unless-stopperd - Always restart unless we (the developers) forcibly stop it
